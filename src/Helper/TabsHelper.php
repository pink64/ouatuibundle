<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 31/03/2016
 * Time: 09:48
 */


namespace Melody\UIBundle\Helper;

class TabsHelper extends BaseHelper {


    protected $tabs = array();

    public function tab_header($name,$label,$icon) {

    }

//    public function tab($name,$icon,$title) {
//        $active = count($this->tabs)==0;
//        $this->tabs[$name] = array('icon'=>$icon,'label'=>$title,'active'=>$active,'id'=>$this->widget_id.'_'.$name);
//
//        return '<div id="'.$this->tabs[$name]['id'].'" class="tab-pane fade in '.($active ? 'active':'').'">' ;
//    }



    public function begin_body() {
        return '<div class="tab-content  ">' ;
    }

    public function end_body() {
        return '</div>' ;
    }

    public function begin_headers() {
        return '<ul class="nav nav-tabs atabs-4" style="height:40px">' ;
    }

    public function end_headers() {
        return '</ul>' ;
    }

    public function header($name,$icon,$title) {
        $active = count($this->tabs)==0;
        $this->tabs[$name] = array('icon'=>$icon,'label'=>$title,'active'=>$active,'id'=>$this->widget_id.'_'.$name);
        $tab = $this->tabs[$name];
        return '<li class="'.($tab['active'] ? 'active':'').'" style="font-size:1em;height:40px"><a data-toggle="tab" href="#'.$tab['id'].'">'.$tab['label'].'</a></li>' ;

    }

    public function begin_tab($name) {
        $tab = $this->tabs[$name];
        return '<div id="'.$tab['id'].'" class="tab-pane fade in '.($tab['active'] ? 'active':'').'">' ;
    }

    public function end_tab() {
        return '</div>' ;
    }


}