<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 31/03/2016
 * Time: 09:48
 */


namespace Melody\UIBundle\Helper;

class AccordionHelper extends BaseHelper {


    protected $tabs = array();


    public function begin_body() {
        return '<div class="panel-group" id="'.$this->widget_id.'">' ;
    }

    public function end_body() {
        return '</div>' ;
    }

    public function begin_panel() {
        return '<div class="panel">' ;
    }

    public function end_panel() {
        return '</div>' ;
    }

    public function begin_panel_heading() {
        return '<div class="panel-heading">' ;
    }

    public function end_panel_heading() {
        return '</div>' ;
    }

    public function begin_panel_body() {
        return '<div class="panel-collapse">' ;
    }

    public function end_panel_body() {
        return '</div>' ;
    }



    public function panel($name,$icon,$title) {
        $active = count($this->tabs)==0;
        $this->tabs[$name] = array('icon'=>$icon,'label'=>$title,'active'=>$active,'id'=>$this->widget_id.'_'.$name);
        $tab = $this->tabs[$name];
        return '<li class="'.($tab['active'] ? 'active':'').'" style="font-size:1em;height:40px"><a data-toggle="tab" href="#'.$tab['id'].'">'.$tab['label'].'</a></li>' ;

    }

    public function begin_tab($name) {
        $tab = $this->tabs[$name];
        return '<div id="'.$tab['id'].'" class="tab-pane fade in '.($tab['active'] ? 'active':'').'">' ;
    }

    public function end_tab() {
        return '</div>' ;
    }


}