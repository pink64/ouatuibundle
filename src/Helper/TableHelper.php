<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 06/06/2016
 * Time: 14:20
 */

namespace Ouat\UIBundle\Helper;

use Ouat\CoreBundle\Formatter\MoneyFormatter;
use Ouat\UIBundle\Helper\TableHelper\Column;
use Ouat\UIBundle\Helper\TableHelper\ColumnAction;
use Ouat\UIBundle\Helper\TableHelper\ColumnField;
use Ouat\UIBundle\Helper\TableHelper\Footer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class TableHelper {

    /**
     * @var ContainerInterface
     */
    protected $container ;

    /**
     * @var array|Column[]
     */
    protected $columns = array();
    protected $items = array();

    /**
     * @var PropertyAccessor
     */
    protected $accessor = NULL ;

    protected $options ;

    public function __construct($options = array()) {

        $this->setOptions($options);

        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    public function setContainer(ContainerInterface $container) {
        $this->container = $container ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'component_id' => null,
            'use_header'    => true,
            'use_footer'    => false,
        ));
    }

    public function getOption($name) {
        return $this->options[$name];
    }

    public function getOptions() {
        return $this->options;
    }

    public function setOptions($options) {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }



    public function getContainer() {
        return $this->container ;
    }

    /**
     * @param $name
     * @return mixed|null|Column
     */
    public function getColumn($name) {
        foreach($this->columns as $column)
            if ($column->getName() == $name)
                return $column ;

        return NULL ;
    }


    protected $footers = array();

    public function addFooter($col_name,$footer_name,$options = array()) {
        $this->getColumn($col_name)->addFooter($footer_name,new Footer($this,$options));
        return $this ;


        if (!isset($this->footers[$col_name]))
            $this->footers[$col_name] = array();

        $this->footers[$col_name][$footer_name] = new Footer($this,$options) ;

        return $this ;
    }

//    public function getColFooters($col_name) {
//        return isset($this->footers[$col_name]) ? $this->footers[$col_name] : array();
//    }
//
//    public function getFooter($col_name,$footer_name) {
//        return $this->footers[$col_name][$footer_name] ;
//    }

    public function getFormatter($name) {
        switch($name) {
            case 'money' : return new MoneyFormatter();
                function ($v,$showIfZero=true,$fmt=2)
                {
                    if (trim($v)=='' || ($v==0 && $showIfZero==false))
                        return '' ;

                    if (!is_numeric($v)) return "NOT A NUMERIC : $v" ;

                    return number_format($v,$fmt,'.',' ').' €';
                } ;
        }
    }


    public function addColumn(Column $column) {
        $column->setBuilder($this);
        $this->columns[] = $column ;

        return $this ;
    }

    public function addField($name,$options = array()) {
        $col = new ColumnField($this,$options);
        $col->setFieldName($name);
        $col->setName($name);

        $this->columns[] = $col ;
        return $this ;
    }

    public function addAction($name,$options = array()) {
        $col = new ColumnAction($this,$options);
        $col->setName($name);


        $this->columns[] = $col;
        return $this ;
    }

    public function getColumns() {
        return $this->columns;
    }

    public function render() {
        $vars = array('th'=>$this);
        return $this->getContainer()->get('twig')->render('OuatUIBundle:helper:tableHelper.html.twig',$vars);
    }

    public function generateRoute($routeName,$routeParameters) {
        return $this->getContainer()->get('router')->generate($routeName,$routeParameters);
    }





}