<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 27/06/2016
 * Time: 10:40
 */

namespace Ouat\UIBundle\Helper;

class OpenUIParams implements \ArrayAccess {

    public static function from($object) {
        if ($object instanceof OpenUIParams)
            return $object ;

        $z = new OpenUIParams();

        foreach($object as $k => $v) {
            error_log("from $k > ".gettype($v));
            $z->{ $k} = $v ;
        }


        return $z ;
    }

    public function offsetSet($offset, $value) {
            $this-> { $offset } = $value;
    }

    public function offsetExists($offset) {
        return isset($this->{ $offset });
    }

    public function offsetUnset($offset) {
    }

    public function offsetGet($offset) {
        return $this->{ $offset };
    }

    public $route ;
    public $parameters ;
    public $args ;
    public $url ;
    public $page ;

    public $in;
    public $text;
    public $icon;
    public $tag = 'a' ;

    public $check;

    protected function findRouteParam($name,$value) {
        if (is_array($this->parameters) && isset($this->parameters[$name]))
            return 'parameters' ;

        if (is_array($this->args) && isset($this->args[$name]))
            return 'args' ;

        return false ;
    }

    public function addRouteParam($name,$value) {
        if ($p = $this->findRouteParam($name,$value))
            $this->{ $p }[$name] = $value ;
        else {
            if (!is_array($this->parameters))
                $this->parameters = array();
            $this->parameters[$name] = $value ;
        }

    }

    public function getRouteParameters($with_expanded_id = false) {

        if (is_array($this->route))
            $route_parameters = isset($this->route[1]) ? $this->route[1] : array();
        else
            $route_parameters = array();

        if (is_array($this->parameters))
            $route_parameters = array_merge($route_parameters,$this->parameters);

        if (is_array($this->args))
            $route_parameters = array_merge($route_parameters,$this->args);

        if ($with_expanded_id == true)
        foreach($route_parameters as $k => $v) {
            if (is_object($v))
                    $route_parameters[$k] = $v->getId();
            }

        return $route_parameters ;
    }



}