<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 31/03/2016
 * Time: 09:47
 */

namespace Melody\UIBundle\Helper;

class BaseHelper {

    protected $widget_id ;

    public function __construct() {
        $this->widget_id = time().rand(0,1000);
    }

    /**
     * @return string
     */
    public function getWidgetId()
    {
        return $this->widget_id;
    }

    
}