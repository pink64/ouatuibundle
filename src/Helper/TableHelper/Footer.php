<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 06/06/2016
 * Time: 14:55
 */

namespace Ouat\UIBundle\Helper\TableHelper;

use Ouat\UIBundle\Helper\TableHelper;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Footer {
    /**
     * @var TableHelper
     */
    protected $builder ;

    /**
     * @var Column
     */
    protected $column ;

    /**
     * @var PropertyAccessor
     */
    protected $accessor = NULL ;

    protected $name ;

    protected $options = array();

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Column
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param Column $column
     */
    public function setColumn($column)
    {
        $this->column = $column;
    }



    public function getLabel() {
        return isset($this->options['label']) ? $this->options['label'] : $this->name ;
    }



    public function __construct($builder,$options) {
        $this->builder = $builder ;
        $this->options = $options ;
        $this->accessor = PropertyAccess::createPropertyAccessor();

        if (isset($this->options['render']) && is_string($this->options['render'])) {
            $this->options['render'] = $this->builder->getFormatter($this->options['render']);
        }
    }

    protected function getFieldValue($item,$fieldName) {
        return $this->accessor->getValue($item,$fieldName);
    }



    public function render() {
        $total = 0 ;
        foreach($this->builder->getItems() as $item)
            $total += $this->getColumn()->getItemValue($item);

        return $total ;
        if (isset($this->options['render']) && $this->options['render']) {
            $value = $this->options['render']($this->builder,$this,$item);
            return $value ;
        }
    }

    public function renderLink($route,$parameters,$in,$icon = 'fa fa-edit',$label = NULL) {
        return '<a href="'. $this->builder->generateRoute($route,$parameters).'" as="'.$in.'"><i class="'.$icon.'"/>&nbsp;'.$label.'</a>' ;
    }
}