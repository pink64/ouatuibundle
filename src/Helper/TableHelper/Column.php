<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 06/06/2016
 * Time: 14:55
 */

namespace Ouat\UIBundle\Helper\TableHelper;

use Ouat\CoreBundle\Formatter\MoneyFormatter;
use Ouat\UIBundle\Helper\TableHelper;
use Symfony\Component\PropertyAccess\PropertyAccess;

class Column {
    /**
     * @var TableHelper
     */
    protected $builder ;

    /**
     * @var PropertyAccessor
     */
    protected $accessor = NULL ;

    protected $name ;

    protected $options = array();

    /**
     * @return TableHelper
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * @param TableHelper $builder
     */
    public function setBuilder($builder)
    {
        $this->builder = $builder;
    }

    


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getLabel() {
        return isset($this->options['label']) ? $this->options['label'] : $this->name ;
    }



    public function __construct($builder,$options) {
        $this->builder = $builder ;
        $this->options = $options ;
        $this->accessor = PropertyAccess::createPropertyAccessor();

        if (isset($this->options['format']) && is_string($this->options['format'])) {
            $this->options['formatter'] = $this->builder->getFormatter($this->options['format']);
        }
    }

    protected function getFieldValue($item,$fieldName) {
        return $this->accessor->getValue($item,$fieldName);
    }

    /**
     * @return mixed|null|MoneyFormatter
     */
    public function getFormatter() {
        return isset($this->options['formatter']) ? $this->options['formatter'] : NULL ;
    }

    public function getHtmlStyle($item) {

        if ($f = $this->getFormatter()) {
            return 'style='.($f->getCellStyle($this->getItemValue($item))).'' ;
        }

        return "" ;
    }


    public function render($item) {
        if (isset($this->options['render']) && $this->options['render']) {
            $value = $this->options['render']($this->builder,$this,$item);
            return $value ;
        }

        return $this->getItemValue($item) ;
    }

    public function renderLink($route,$parameters,$in,$icon = 'fa fa-edit',$label = NULL) {
        return '<a href="'. $this->builder->generateRoute($route,$parameters).'" in="'.$in.'" on-back="refresh"><i class="'.$icon.'"/>&nbsp;'.$label.'</a>' ;
    }


    protected $footers = NULL ;

    public function addFooter($name,Footer $footer) {
        $this->footers[$name] = $footer ;

        $footer->setColumn($this);
        return $this ;
    }

    public function getFooters() {
        return $this->footers ;
    }

}