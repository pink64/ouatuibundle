<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 06/06/2016
 * Time: 14:56
 */


namespace Ouat\UIBundle\Helper\TableHelper;

use Ouat\EntityBundle\Interfaces\HasStyleInterface;

class ColumnField extends Column {

    protected $fieldName ;

    protected $render_item ;

    /**
     * @return mixed
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param mixed $fieldName
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    /**
     * @return mixed
     */
    public function getRenderItem()
    {
        return $this->render_item;
    }

    /**
     * @param mixed $render_item
     */
    public function setRenderItem($render_item)
    {
        $this->render_item = $render_item;
    }

    public function getItemValue($item) {
        return $this->getFieldValue($item,$this->fieldName);
    }


    public function render($item) {
        $value = $this->getItemValue($item);

        if ($value instanceof \DateTime)
            return $value->format('d/m/Y');

        if ($value instanceof HasStyleInterface) {
            return $this->stylify($value);
        }

        if ($value instanceof \Traversable) {
            $rows = array();

            $fn_render_item = isset($this->options['render_item']) && $this->options['render_item'] ?
                                $this->options['render_item'] :
                                function($builder, $col, $record,$item) { return '!'. $item ; }
                            ;

            foreach($value as $sub_item) {
                $rows[] = '<div>'.$fn_render_item($this->builder,$this,$item,$sub_item).'</div>' ;
            }


            return implode('',$rows);
        }

        if (isset($this->options['formatter']) && $this->options['formatter']) {
            $value = $this->options['formatter']->formatHTML($value);
            return $value ;
        }

        return $value ;
    }

    public function stylify($value,$style = null,$tag = 'span') {
        if ($style===NULL && is_object($value) && $value instanceof HasStyleInterface)
            $style = $value->getStyle();

        if (!$style)
            return $value ;

        return '<'.$tag.' style="border-radius:10px;padding:5px;'.$style->toHtmlStyle().'">'.$value.'</'.$tag.'>' ;
    }



}