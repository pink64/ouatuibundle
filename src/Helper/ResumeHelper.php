<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 31/03/2016
 * Time: 09:48
 */


namespace Melody\UIBundle\Helper;

class ResumeHelper extends BaseHelper {

    protected $groups = array();

    protected function get_current_group() {
        return $this->groups[count($this->groups)-1] ;
    }

    public function title($icon,$label) {
        return '<h6 style="border-bottom: solid 1px #bdbdbd;margin-bottom: 10px;margin-top:10px;color:#bdbdbd;font-weight: normal">'.$label.'</h6>' ;
    }

    public function begin_group($options = array()) {
        $default = array(
            'size' => 6,
            'label_size' => 4,
        );
        $group = array_merge($default,$options);
        if (!isset($group['value_size'])) $group['value_size'] = 12 - $group['label_size'] ;
        $this->groups[] = $group ;

        return '<div class="col-lg-'.$group['size'].'">' ;
    }

    public function end_group() {
        array_pop($this->groups);
        return '</div>' ;
    }

    public function resume_label($label) {
        $group = $this->get_current_group();

        if (!$group['label_size'])
            return '';

        return '<label class="col-lg-'.$group['label_size'].'">'.$label.'</label>' ;
    }

    public function resume_value($value) {
        $group = $this->get_current_group();
        if (!$group['value_size'])
            return '';

        return '<div class="col-lg-'.$group['value_size'].' " style="font-weight: bold;color: #1C2331 ">'.$value.'</div>' ;
    }

    public function resume_field($label,$value) {
        return '<div class="row">'.$this->resume_label($label).$this->resume_value($value).'</div>';
    }


}