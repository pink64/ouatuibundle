<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 15/05/2015
 * Time: 15:16
 */
namespace Ouat\UIBundle\Tools;



class AppUIService {

    protected $logs = array(

    );


    public function toHTML() {
        return '<div id="sc_meta_response" style="display:none">'.$this->toJSON().'</div>' ;
    }

    public function toJSON() {
        $res = array();
        $res['logs'] = $this->logs ;

        return json_encode($res);
    }

    protected function addLog($level,$text,$icon) {
        $this->logs[] = array('level'=>$level,'icon'=>$icon,'text'=>$text);
        return $this ;
    }

    public function log($level,$text,$icon = NULL) {
        return $this->addLog($level, $text, $icon);
    }

    public function notice($text,$icon = NULL) {
        return $this->addLog('notice',$text,$icon);
    }

    public function warning($text,$icon = NULL) {
        return $this->addLog('warning',$text,$icon);
    }

    public function error($text,$icon = NULL) {
        return $this->addLog('error',$text,$icon);
    }

    public function success($text,$icon = NULL) {
        return $this->addLog('success',$text,$icon);
    }

    public function danger($text,$icon = NULL) {
        return $this->addLog('danger',$text,$icon);
    }
}