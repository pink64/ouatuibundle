<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 18/02/2016
 * Time: 18:06
 */

namespace Melody\UIBundle\Tools;

class OX {

    /**
     * Formate la définition d'une icone
     * @param $icon array|string
     *
     * Si c'est une chaine : type:name:size:color
     */
    public static function formatIcon($icon,$defaults = array()) {
        if(is_string($icon)) {
            $res = explode(':',$icon);
            $icon = array();
            if ($res[0]) $icon['type'] = $res[0] ;
            $icon['name'] = $res[1] ;
            if (isset($res[2])) $icon['size'] = $res[2] ;
            if (isset($res[3])) $icon['color'] = $res[3] ;
        }

        return array_merge(array('type'=>'md','size'=>'md','color'=>''),$defaults,$icon);

    }


    /**
     * @param $icon array|string
     */
    protected static function ouat_icon_md($icon) {

        $ics = array('material-icons');

        $sizes = array(
            'xs' => array('fs'=>'12px','ic'=>'md-18'),
            'sm' => array('fs'=>'18px','ic'=>'md-18'),
            'md' => array('fs'=>'24px','ic'=>'md-24'),
            'lg' => array('fs'=>'36px','ic'=>'md-36'),
            'xlg' => array('fs'=>'48px','ic'=>'md-48'),
        );

        $colors = array(
            'ok' => array('fg'=>'green'),
            'warning' => array('fg'=>'orange'),
            'error' => array('fg'=>'red'),
            'default' => array('fg'=>'#1C2331')
        );

        $fs = '' ;
        $fs = isset($sizes[$icon['size']]) ? $sizes[$icon['size']]['fs'] : $icon['size'] ;
        if (isset($sizes[$icon['size']]['ic'])) {
            $ics[] = $sizes[$icon['size']]['ic'] ;
        }


        $fg = isset($colors[$icon['color']]) ? $colors[$icon['color']]['fg'] : $icon['color'];

        $html = "" ;

//        $icon['stacked'] = 'circle' ;
        $ic = '' ;

        if (isset($icon['stacked']) && $icon['stacked']) {
            $html.='<span class="fa-stack fa-lg">
                <i class="fa fa-'.$icon['stacked'].' fa-stack-2x" style="color:'.$fg.'"></i>' ;
            $fg = 'white' ;
            $ic = ' fa-inverse' ;
            $ics[] = 'fa-stack-1x fa-inverse' ;
        }

        $html.='<i class="'.implode(' ',$ics).'" style="vertical-align:middle;font-size:'.$fs.';color:'.$fg.'">'.$icon['name'].'</i>' ;

        if (isset($icon['stacked'])) {
            $html.="</span>" ;
        }

        return $html ;
    }


    protected static function ouat_icon_fa($icon) {

        $sizes = array(
            'xs' => array('fs'=>'8pt'),
            'sm' => array('fs'=>'10pt'),
            'md' => array('fs'=>'12pt'),
            'lg' => array('fs'=>'14pt'),
            'xlg' => array('fs'=>'16pt'),
        );

        $colors = array(
            'ok' => array('fg'=>'green'),
            'warning' => array('fg'=>'orange'),
            'error' => array('fg'=>'red'),
        );

        $fs = isset($sizes[$icon['size']]) ? $sizes[$icon['size']]['fs'] : $icon['size'] ;
        $fg = isset($colors[$icon['color']]) ? $colors[$icon['color']]['fg'] : $icon['color'];

        $html = "" ;

        $icon['stacked'] = 'circle' ;
        $ic = '' ;

        if (isset($icon['stacked'])) {
            $html.='<span class="fa-stack fa-lg">
                <i class="fa fa-'.$icon['stacked'].' fa-stack-2x" style="color:'.$fg.'"></i>' ;
            $fg = 'white' ;
            $ic = 'fa-stack-1x fa-inverse' ;

        }

        $html.='<i class="fa fa-fw fa-'.$icon['name'].' '.$ic.'" style="font-size:'.$fs.';color:'.$fg.'"></i>' ;

        if (isset($icon['stacked']) && $icon['stacked']) {
            $html.="</span>" ;
        }

        return $html ;
    }

    /**
     * @param $icon array|string
     */
    public static function ouat_icon($icon,$defaults = array()) {
        $icon = OX::formatIcon($icon,$defaults);

        switch($icon['type']) {
            case 'fa' : return OX::ouat_icon_fa($icon);
            case 'md' : return OX::ouat_icon_md($icon);
        }
    }


    /**
     * <a
    href="{{ path('fbt_app_admin_user_index',{ user: item.id }) }}"
    ox="modal"
    {#ox-route = "fbt_pit_planpit_index"#}
    oxp-pit="{{ item.id }}"
    oxo-modal-size="1000x500"
    oxo-close_success="refresh"
    class="btn-floating waves-effect waves-light blue btn-extra-small"><i class="material-icons">edit</i> </a>
     */
    public static function ox_generate_attributes($args) {
        $attributes = array();

        foreach($args as $ka => $va) {

            // traitement d'un tableau d'options
            if (substr($ka,0,4)=='oxo-') {
                $sfx = substr($ka,4);
                if (is_string($va))
                    $attributes[$ka] = $va ;
                else
                    foreach($va as $ko => $vo) {
                        $attributes['oxo-'.$sfx.'-'.$ko] = $vo ;
                    }

            }

            switch($ka) {
                case 'oxp' :
                    foreach($va as $kp => $vp) {
                        $attributes['oxp-'.$kp] = $vp ;
                    }
                    break ;
                case 'ox' :
                    $attributes['ox'] = $va ;
                    break ;
            }
        }

        $attributes['ox'] = $args['ox'] ;

        return $attributes ;


    }

    /**
     * @param $url_or_route
     * @param $modal_options
     * @param $other_options
     * <a
    href="{{ path('fbt_app_admin_user_index',{ user: item.id }) }}"
    ox="modal"
    {#ox-route = "fbt_pit_planpit_index"#}
    oxp-pit="{{ item.id }}"
    oxo-modal-size="1000x500"
    oxo-close_success="refresh"
    class="btn-floating waves-effect waves-light blue btn-extra-small"><i class="material-icons">edit</i> </a>
     */
    public static function ox_open_modal($url_or_route,$modal_options,$other_options) {
        $args = array();
        $args['ox'] = 'modal' ;
        //$args['href'] =

    }


    /**
     *
     * <a
    href="{{ path('fbt_app_admin_user_index',{ user: item.id }) }}"
    ox="modal"
    {#ox-route = "fbt_pit_planpit_index"#}
    oxp-pit="{{ item.id }}"
    oxo-modal-size="1000x500"
    oxo-close_success="refresh"
    class="btn-floating waves-effect waves-light blue btn-extra-small"><i class="material-icons">edit</i> </a>
     *
     * @param $link_option
     * @param $href
     * @param $modal_options
     * @param $other_options
     */
    public static function ox_link_modal($link_option,$href,$modal_options,$other_options = array()) {
        $html = '<a href="'.$href.'" ' ;
        $html.= ' class="btn-floating waves-effect waves-light blue btn-extra-small"' ;

        $other_options['ox'] = 'modal' ;
        $other_options['modal'] = $modal_options ;
        $ox_attributes = self::ox_generate_attributes($other_options);

        foreach($ox_attributes as $ko => $vo) {
            $html.= ' '.$ko.'="'.$vo.'"' ;
        }
        $html.= ' >' ;
        $html.= $link_option['label'] ;

        $html.= '</a>' ;
        return $html ;
    }
}