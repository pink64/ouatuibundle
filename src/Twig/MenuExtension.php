<?php

/*
 * This file is part of the MopaBootstrapBundle.
 *
 * (c) Philipp A. Mohrenweiser <phiamo@googlemail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ouat\UIBundle\Twig;
use Ouat\UIBundle\Menu\Converter\MenuConverter;


/**
 * Twig Extension for rendering a Bootstrap menu.
 *
 * This function provides some more features
 * than knp_menu_render, but does more or less the same.
 *
 * @author phiamo <phiamo@googlemail.com>
 */
class MenuExtension extends \Mopa\Bundle\BootstrapBundle\Twig\MenuExtension
{
    /**
     * @return MenuConverter
     */
    protected function getMenuConverter()
    {
       
        if ($this->menuConverter === null) {
            $this->menuConverter = new MenuConverter();
        }

        return $this->menuConverter;
    }


}
