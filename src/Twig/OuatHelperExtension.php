<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 05/03/2015
 * Time: 15:24
 */
namespace Ouat\UIBundle\Twig;


use Melody\UIBundle\Helper\AccordionHelper;
use Melody\UIBundle\Helper\ResumeHelper;
use Melody\UIBundle\Helper\TabsHelper;
use Melody\UIBundle\Tools\OX;
use Ouat\UIBundle\ListBuilder\ListBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OuatHelperExtension extends \Twig_Extension implements ContainerAwareInterface
{
    public function __construct(ContainerInterface $container) {
        $this->setContainer($container);
    }
    /**
     * @var ContainerInterface $container
     */
    protected $container ;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getName()
    {
        return 'melody_helper_extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('tabs_create', array($this, 'tabs_create'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_begin_headers', array($this, 'tabs_begin_headers'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_end_headers', array($this, 'tabs_end_headers'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_header', array($this, 'tabs_header'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_begin_body', array($this, 'tabs_begin_body'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_end_body', array($this, 'tabs_end_body'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_begin_tab', array($this, 'tabs_begin_tab'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('tabs_end_tab', array($this, 'tabs_end_tab'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('accordion_create', array($this, 'accordion_create'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_begin_headers', array($this, 'accordion_begin_headers'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_end_headers', array($this, 'accordion_end_headers'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_header', array($this, 'accordion_header'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_begin_body', array($this, 'accordion_begin_body'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_end_body', array($this, 'accordion_end_body'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_begin_tab', array($this, 'accordion_begin_tab'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('accordion_end_tab', array($this, 'accordion_end_tab'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('resume_create', array($this, 'resume_create'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_title', array($this, 'resume_title'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_begin_group', array($this, 'resume_begin_group'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_end_group', array($this, 'resume_end_group'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_label', array($this, 'resume_label'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_value', array($this, 'resume_value'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('resume_field', array($this, 'resume_field'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('th_render_list', array($this, 'renderList'), array('is_safe' => array('html'))),
        );
    }

    /**
     * @param $builder ListBuilder
     * @throws \InvalidArgumentException
     * @return string
     */
    public function renderList($builder,  $data, $fields,array $options = array())
    {
        if (is_string($builder)) {
            list($bundle_name,$builder_name) = explode(':',$builder);
            $class = $bundle_name.'\\ListBuilder\\'.$builder_name ;
            $builder = new $class($this->container);

        }

        $builder->start();
        if ($data)
            $builder->getTable()->setItems($data);

        foreach($fields as $field)
            $builder->add($field);


        return $builder->getTable()->render();
    }

    //<editor-fold desc="Tabs Helper" >

    public function tabs_create() {
        return new TabsHelper();
    }

    public function tabs_begin_headers(TabsHelper $tab) {
        return $tab->begin_headers();
    }

    public function tabs_end_headers(TabsHelper $tab) {
        return $tab->end_headers();
    }

    public function tabs_header(TabsHelper $tab,$name,$icon,$label) {
        return $tab->header($name,$icon,$label);
    }

    public function tabs_begin_body(TabsHelper $tab) {
        return $tab->begin_body();
    }

    public function tabs_end_body(TabsHelper $tab) {
        return $tab->end_body();
    }

    public function tabs_begin_tab(TabsHelper $tab,$name) {
        return $tab->begin_tab($name);
    }

    public function tabs_end_tab(TabsHelper $tab) {
        return $tab->end_tab();
    }

    //</editor-fold>

    //<editor-fold desc="Resume Helper">

    public function resume_create() {
        return new ResumeHelper();
    }

    public function resume_title(ResumeHelper $helper,$icon,$label) {
        return $helper->title($icon,$label);
    }

    public function resume_begin_group(ResumeHelper $helper,$options = array()) {
        return $helper->begin_group($options);
    }

    public function resume_end_group(ResumeHelper $helper) {
        return $helper->end_group();
    }

    public function resume_label(ResumeHelper $helper,$label) {
        return $helper->resume_label($label);
    }

    public function resume_value(ResumeHelper $helper,$value) {
        return $helper->resume_value($value);
    }

    public function resume_field(ResumeHelper $helper,$label,$value) {
        return $helper->resume_field($label,$value);
    }

    //</editor-fold>

    //<editor-fold desc="Accordion Helper" >

    public function accordion_create() {
        return new AccordionHelper();
    }

    public function accordion_begin_headers(AccordionHelper $tab) {
        return $tab->begin_headers();
    }

    public function accordion_end_headers(AccordionHelper $tab) {
        return $tab->end_headers();
    }

    public function accordion_header(AccordionHelper $tab,$name,$icon,$label) {
        return $tab->header($name,$icon,$label);
    }

    public function accordion_begin_body(AccordionHelper $tab) {
        return $tab->begin_body();
    }

    public function accordion_end_body(AccordionHelper $tab) {
        return $tab->end_body();
    }

    public function accordion_begin_tab(AccordionHelper $tab,$name) {
        return $tab->begin_tab($name);
    }

    public function accordion_end_tab(AccordionHelper $tab) {
        return $tab->end_tab();
    }

    //</editor-fold>

}