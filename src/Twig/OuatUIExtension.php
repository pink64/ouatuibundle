<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 05/03/2015
 * Time: 15:24
 */
namespace Ouat\UIBundle\Twig;


use JMS\Serializer\SerializationContext;
use Melody\UIBundle\Helper\TabsHelper;
use Ouat\UIBundle\Screen\ScreenConfig;
use Ouat\UIBundle\Tools\OX;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarDumper\VarDumper;

class OuatUIExtension extends \Twig_Extension implements ContainerAwareInterface
{
    public function __construct(ContainerInterface $container) {
        $this->setContainer($container);
    }
    /**
     * @var ContainerInterface $container
     */
    protected $container ;

    public function setContainer(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getName()
    {
        return 'melody_ui_extension';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('link_prepare_options', array($this, 'link_prepare_options')),
            new \Twig_SimpleFunction('merge', array($this, 'merge')),
            new \Twig_SimpleFunction('is_recent', array($this, 'is_recent')),
            new \Twig_SimpleFunction('generate_id', array($this, 'generate_id')),
            new \Twig_SimpleFunction('trim', array($this, 'trim')),
            new \Twig_SimpleFunction('periode', array($this, 'periode')),
            new \Twig_SimpleFunction('get_service', array($this, 'get_service')),
            new \Twig_SimpleFunction('guess_screen_layout', array($this, 'guess_screen_layout')),
            new \Twig_SimpleFunction('get_screen_layout', array($this, 'get_screen_layout')),
            new \Twig_SimpleFunction('get_screen_page_layout', array($this, 'get_screen_page_layout')),
            new \Twig_SimpleFunction('get_page_layout', array($this, 'get_page_layout')),
            new \Twig_SimpleFunction('get_action_layout', array($this, 'get_action_layout')),
            new \Twig_SimpleFunction('guess_screen_js', array($this, 'guess_screen_js')),
            new \Twig_SimpleFunction('samedir', array($this, 'samedir')),
            new \Twig_SimpleFunction('gettype', array($this, 'gettype')),
            new \Twig_SimpleFunction('attribute_set', array($this, 'attribute_set')),
            new \Twig_SimpleFunction('sc_build_workspace_options', array($this, 'sc_build_workspace_options')),
            new \Twig_SimpleFunction('sc_build_modal_options', array($this, 'sc_build_modal_options')),
            new \Twig_SimpleFunction('xui_create_accordion', array($this, 'xui_create_accordion')),
            new \Twig_SimpleFunction('ouat_icon', array($this, 'ouat_icon'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('jsonDecode', array($this, 'jsonDecode'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('render_document_actions', array($this, 'render_document_actions'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('getScreenConfig', array($this, 'getScreenConfig'), array()),



            new \Twig_SimpleFunction('ox_open_document', array($this, 'ox_open_document'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('create_tabs', array($this, 'createTabsHelper'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('ox_link_modal', array($this, 'ox_link_modal'), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction('ox_generate_attributes', array($this, 'ox_generate_attributes'), array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('date_array_to_string', array($this, 'date_array_to_string'),array('is_safe' => array('html'))),

            new \Twig_SimpleFunction('get_document_actions', array($this, 'get_document_actions')),
            new \Twig_SimpleFunction('get_origin', array($this, 'get_origin')),



        );
    }

    public function get_origin() {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $params = explode('::',$request->attributes->get('_controller'));
        $controller_name = $params[0];

        $session = $this->container->get('session');
        $key_name = 'origin_'.$controller_name ;

        if ($session->has($key_name))
            return $session->get($key_name);

        return NULL ;
    }

    public function attribute_set($data,$key,$value) {
        $data[$key] = $value ;
    }

    public function xui_create_accordion() {
        $res = array();

        $res['id'] = 'accordion_'.rand(0,1000).time();
        $res['panel'] = 0 ;

        return $res ;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('mailto', array($this, 'mailtoFilter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('date2', array($this, 'date2Filter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('month', array($this, 'monthFilter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('money', array($this, 'moneyFilter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('money2', array($this, 'money2Filter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('duree', array($this, 'dureeFilter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('pcent', array($this, 'pcentFilter'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('enumerable', array($this, 'enumerable'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('render_attributes', array($this, 'render_attributes'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('color_preview', array($this, 'color_preview'),array('is_safe' => array('html'))),
            new \Twig_SimpleFilter('jms_array', array($this, 'jms_array'),array('is_safe' => array('all'))),



//         new \Twig_SimpleFilter('ouat_icon', array($this, 'ouat_icon', array('is_safe' => array('html')))),
        );
    }

    protected $last_document ;
    protected $last_document_metadata ;

    public function jms_array($data) {
        $ser = $this->container->get('jms_serializer');
        return $ser->toArray($data,SerializationContext::create()->enableMaxDepthChecks());
    }

    public function jsonDecode($str) {
        return json_decode($str);
    }

    function get_service($service_name) {
        return $this->container->get($service_name) ;
    }

    function date_array_to_string($value) {
        if (is_string($value)) return $value ;
        if (!$value) return '' ;
        if (!$value['year']) return '' ;

        return sprintf("%02d/%02d/%04d",$value['day'],$value['month'],$value['year']);
    }



    function get_action_layout($template) {
        $inf = explode(':',$template->getTemplateName());
        $inf['2'] = 'action.html.twig' ;
        return implode(':',$inf);
    }

    function guess_screen_js($template) {
        $inf = explode(':',$template->getTemplateName());
        // [0] : nom du bundle
        // [1] : chemin du controlleur (Workspace\...\...\Controlleur)
        $pth = explode('\\',$inf[1]);

        return implode('_',$pth);
    }

    function samedir($template,$name) {
        $inf = explode(':',$template->getTemplateName());
        $inf['2'] = $name ;
        return implode(':',$inf);
    }

    function get_page_layout($template) {
        $inf = explode(':',$template->getTemplateName());
        $inf['2'] = 'page.html.twig' ;
        return implode(':',$inf);
    }

    function get_screen_layout($template)
    {
        if ($template != 'auto') {
            $inf = explode(':', $template->getTemplateName());
            $inf['2'] = 'screen_layout.html.twig';
            return implode(':', $inf);
        }

        if ($template == 'auto') {
            $rs = $this->container->get('request_stack');
            $rq = $rs->getCurrentRequest();
            $controller = $rq->get('_controller');

            $template = $this->controllerNameToTemplate($controller).'screen_layout.html.twig';

            return $template ;

        }
    }

    function guess_screen_layout($default='module')
    {
        $rs = $this->container->get('request_stack');
        $rq = $rs->getMasterRequest();

        $rps = $rq->get('_route_params');

        if ($rps && isset($rps['_layout']))
            return $rps['_layout'] ;

        return $default ;
    }

    function get_screen_page_layout($template) {

        if ($template != 'auto') {
            $inf = explode(':',$template->getTemplateName());
            $inf['2'] = 'screen_page_layout.html.twig' ;
            return implode(':',$inf);
        }

        if ($template == 'auto') {
            $rs = $this->container->get('request_stack');
            $rq = $rs->getCurrentRequest();
            $controller = $rq->get('_controller');
//            error_log($this->controllerNameToTemplate($controller));
            $template = $this->controllerNameToTemplate($controller).'screen_page_layout.html.twig';

           return $template ;

        }


    }

    /**
     * AppBundle\Controller\AdminController::listeSectionAnalytiqueAction
     * @param $controllerName
     */
    protected function controllerNameToTemplate($controllerName) {
        $inf = explode('::',$controllerName);
        $gs = $this->container->get('sensio_framework_extra.view.guesser');

        list($bundle_ns,$cmp) = $this->extractSfComponents('Controller',$inf[0]);

        return '@'.str_replace('Bundle','',$bundle_ns).'/'.$cmp.'/' ;
        //$ctrl = str_replace('Controller'$inf[0]);
    }

    protected function extractSfComponents($componentType,$className) {
        $inf = explode('\\',$className);
        $found = false ;


        $k = strpos($className,$motif = '\\'.$componentType.'\\');

        $ns_bundle = str_replace('\\','',substr($className,0,$k));
        $cmp_path = substr($className,strlen($motif)+$k);
        $cmp_path = str_replace($componentType,'',$cmp_path);
        return array($ns_bundle,$cmp_path);


    }

    /**
     * @param $document
     * @return \Ouat\AppBundle\Document\DocumentMetadata
     */
    function get_metadata($document) {

        if ($document === $this->last_document)
            return $this->last_document_metadata ;

        $app = $this->container->get('the_app');
        $meta = $app->getDocumentMetadata($document);

        $this->last_document = $document ;
        $this->last_document_metadata = $meta ;

        return $meta ;

    }

    function doctypeCSSFilter($document) {
        $meta = $this->get_metadata($document);
        return $meta->getCss();
    }

    function date2Filter($v) {

        if ($v==NULL) return '' ;

        $z = new \DateTime();
        if ($v instanceof \DateTime)
            return $v->format('d/m/Y');

        if (is_array($v)) {
            if (!$v['day']) return '' ;
            return sprintf("%02d/%02d/%04d",$v['day'],$v['month'],$v['year']);
        }

        if (trim($v)=='')
            return '' ;

        $tmp=explode(' ',trim($v));
        $v=$tmp[0];
        $tmp = @explode('-',$v);

        if (count($tmp)==3)
        {
            list($y,$m,$d) = $tmp ;
            return sprintf("%02d/%02d/%04d",$d,$m,$y);
        }

        $tmp = @explode('/',$v);
        if (count($tmp)==3)
        {
            list($d,$m,$y) = $tmp ;
            return sprintf("%02d/%02d/%04d",$d,$m,$y);
        }

        return '?'.$v ;
    }

    public function moneyFilter($v,$showIfZero=true,$fmt=2)
    {
        if (trim($v)=='' || ($v==0 && $showIfZero==false))
            return '' ;

        if (!is_numeric($v)) return "NOT A NUMERIC : $v" ;

        return number_format($v,$fmt,'.',' ').' €';
    }

    public function money2Filter($v,$showIfZero=true,$fmt=2)
    {
        if ($v === NULL) $v = 0 ;
        if (!is_numeric($v)) return "NOT A NUMERIC : $v" ;
        elseif (trim($v)=='' || ($v==0 && $showIfZero==false))
            $v='';
        else
            $v=number_format($v,$fmt,'.',' ');



        $html = '<div class="input-group"><div class="form-control text-right">'.$v.'</div><span class="input-group-addon">€</span></div>';

        return $html;
    }

    public function mailtoFilter($email,$label=NULL)
    {
        if ($label === NULL) $label = $email ;

        return '<a href="mailto:'.$email.'">'.$label.'</a>' ;
    }

    public function pcentFilter($v,$showIfZero=true,$fmt=2)
    {
        if (trim($v)=='' || ($v==0 && $showIfZero==false))
            return '' ;

        return number_format(100*$v,$fmt,'.',' ').' %';
    }

    public function periode($d1,$d2)
    {
        if ($d1 && $d2) {
            return 'du '.$this->date2Filter($d1).' au '.$this->date2Filter($d2);
        }

        if ($d1 && !$d2) {
            return 'à partir du '.$this->date2Filter($d1);
        }

        if (!$d1 && $d2) {
            return "jusqu'au ".$this->date2Filter($d2);
        }

        return '' ;
    }

    public function monthFilter($val)
    {
        $inf = explode('-',$val);
        if (count($inf)!=2) return NULL ;

        $mth = array('janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre');
        return $mth[(int)$inf[1]-1].' '.$inf[0];
    }

    public function openDocumentURL($document,$route = NULL,$args=array())
    {
        $meta = $this->get_metadata($document);

        $args = array_merge($meta->getDialogRouteParameters(),$args);
        $url = $this->container->get('router')->generate($route ? $route : $meta->getDialogRoute(),$args);

        return $url ;

    }

    public function openDocumentFilter($document,$route = NULL,$args=array())
    {
        $meta = $this->get_metadata($document);

        $args = array_merge($meta->getDialogRouteParameters(),$args);
        $url = $this->container->get('router')->generate($route ? $route : $meta->getDialogRoute(),$args);

        $html = '' ;
        $html.= ' href='.$url.'' ;
        $html.= ' sc-open-document='.$meta->getTopmostDocument().'' ;
        $html.= ' ' ;

        return $html;
    }

    public function badgeFilter($document,$options =array())
    {
        $meta = $this->get_metadata($document);

        $url = $this->container->get('router')->generate($meta->getDialogRoute(),$meta->getDialogRouteParameters());

        if (isset($options['badge_type']))
            $templateFile = $meta->getTemplateFile('badge_'.$options['badge_type']);
        else
        $templateFile = $meta->getTemplateFile('badge');

        $attr = array();
        if (isset($options['reload'])) $attr[] = 'o_reload="'.($options['reload'] ? 'true':'false').'"' ;

        $html = '<div  id="'.$meta->getDocumentName().'" sc-docname="'.$meta->getDocumentName().'" href="'.$url.'" sc-open-document="'.$meta->getTopmostDocument().'" '.implode(' ',$attr).'>' ;
        $html.= $this->container->get('twig')->render($templateFile,array('item'=>$document));
        $html.= '</div>' ;
        return $html;


    }




    function resolve_document_route($document) {
        $route = array();
        $this->container->get('documents_route_resolver')->resolveRoute($document,$route);

        return $route ;
    }

    function merge($v1,$v2) {
        return array_merge($v1,$v2);
    }

    function generate_id() {
        return ''.rand(0,1000).time();
    }

    function trim($value) {
        return trim(''.$value);
    }

    function sc_link_attributes($options) {
        $res = array(
            'sc_tag' => 'sc-open sc-target-self',
            'target' => '',
            'url' => '#'
        );

        if (isset($options['page']))
            $res['sc_tag'] = 'sc-open-page='.$options['page'] ;

        if (isset($options['target'])) {
            $res['sc_tag'] = '' ;
            $res['target'] = 'target="'.($res['target'] ? $res['target'] : '_blank').'"' ;
        }

        if (isset($options['modal']))
            $res['sc_tag'] = 'modal ' ;

        if (isset($options['route'])) {
            $route_args = isset($options['route_parameters']) ? $options['route_parameters'] : array();
            $res['url'] = $this->container->get('router')->generate($options['route'], $route_args);
        }

        return $res ;

    }

    /**
     * Prépare le tableau des options pour créer un lien
     *
     * @param $icon
     * @param $label
     * @param $route
     * @param $parameters
     * @param $credential
     * @param array $options
     * @return string
     */
    public function sc_link_prepare($icon,$label,$route,$parameters,$credential,$options = array()) {
        $link_options = array();
        $link_options['route'] = $route ;
        $link_options['route_parameters'] = $parameters ;

        $lk_options = $this->sc_link_attributes($link_options) ;

        $res = array('attributes'=>array());

        foreach(explode(' ',$lk_options['sc_tag']) as $tag)
        $res['attributes'][$tag] = "";

        $res['attributes']['href'] = $lk_options['url'] ;
        $res['attributes']['url'] = $lk_options['url'] ;
        $res['label'] = $label ;
        $res['icon'] = $icon ;

        if (isset($res['attributes']['url']) && !isset($res['href']))
            $res['attributes']['href'] = $res['attributes']['url'] ;


        return $res  ;
    }

    public function gettype($object) {
        return gettype($object);
    }


    public function sc_build_workspace_options($context) {
        $default = array();
        $default['layout_top']    = false ;
        $default['layout_south']  = false ;
        $default['layout_west']   = true  ;


        return $default ;
    }


    /**
     * @param $context
     *
     * panel: false|size|size%     Faux (pour aucun panneau) ou une largeur
     *
     *
     * @return array
     *
     *  panel =>
     *      size
     *
     */
    public function sc_build_modal_options($context) {
        $default = array();
        $default['layout_top']    = true ;
        $default['layout_south']  = false ;
        $default['layout_west']   = true  ;
        $default['navbar'] = false ;

        return array_merge($default,$context) ;
        $ui = isset($context['_ui']) && $context['_ui'] ? $context['_ui'] : array() ;

        if (!isset($ui['panel']))
            $ui['panel'] = true ;

        if ($ui['panel']===false) {

            $ui['panel_col'] = 0 ;
        }

        $default = array(
            'panel' => true,
            'panel_col' => 3,
            'content_col' => 9,
            'width' => '100%',
            'form' => NULL
        );

        $res = array_merge($default,$ui);

        $res['content_col'] = 12 - $res['panel_col'] ;

        return $res ;
    }

    public function buildScreenTemplateName($name) {
        $request = $this->container->get('request');
        $bundle_name = $request->attributes->get('_template')->get('bundle');
        $action_name = $request->attributes->get('_template')->get('controller');

        return $bundle_name.':'.$action_name.':'.$name ;

    }

    protected function safe_read($obj,$name,$default = NULL) {

    if (is_object($obj))
        return method_exists($obj,$m = 'get'.ucfirst($name)) ? $obj->{ $m}() : $default;
        else return isset($obj[$name]) ? $obj[$name] : $default;
    }

    public function link_prepare_options($options) {
        $res  = array(
            'attributes' => array()
        );

        if (isset($options['route_parameters']) && !isset($options['routeParameters']))
            $options['routeParameters'] = $options['route_parameters'];



        if ($v = $this->safe_read($options,'cmd'))
            $res['attributes']['cmd'] = $v ;

        if ($v = $this->safe_read($options,'args')) {
            if (is_array($v)) {
                foreach($v as $sk => $sv)
                    $res['attributes']['a_'.$sk] = $sv ;
            }
        }

        if ($v = $this->safe_read($options,'options')) {
            if (is_array($v)) {
                foreach($v as $sk => $sv)
                    $res['attributes']['o_'.$sk] = $sv ;
            }

        }


        if ($uri  = $this->safe_read($options,'uri')) {
            $res['attributes']['href'] = $uri ;
        }
        elseif ($uri  = $this->safe_read($options,'route')) {
            $route_params = $this->safe_read($options,'route_parameters',array());

            if (is_array($uri)) {
                $route_params = isset($uri['parameters']) ? $uri['parameters'] : array();
                $uri = $uri['route'];
            }

            $res['attributes']['href'] = $this->container->get('router')->generate($uri,$route_params);
        }

        if ($target  = $this->safe_read($options,'target')) {

            @list($target_type,$target_name) = explode(':',$target);

            if (!$target_name) $target_name = 'default';

            switch($target_type) {
                case '!workspace' :
//                    $res['attributes']['href'] = null ;
                    $res['attributes']['sc-workspace-open'] = $target_name;
                    break ;

                case '!modal' :
                    $res['attributes']['xtarget'] = '!modal';
                    break ;

                case '_blank' :
                    $res['attributes']['target'] = '_blank';
                    break ;
                default:
                    $res['attributes']['xtarget'] = $target;
            }
        }



        return $res ;
    }


    function is_recent($date) {
        return false ;


    }

    public function color_preview($value) {
        if (!$value) return "";

        return '<span style="display:inline-block;width:20px;background-color:'.$value->getBackColor().';color:'.$value->getTextColor().'">&nbsp;</span>' ;


    }

//    /**
// * Formate la définition d'une icone
// * @param $icon array|string
// *
// * Si c'est une chaine : type:name:size:color
// */
//    public function formatIcon($icon,$defaults = array()) {
//        if(is_string($icon)) {
//            $res = explode(':',$icon);
//            $icon = array();
//            if ($res[0]) $icon['type'] = $res[0] ;
//            $icon['name'] = $res[1] ;
//            if (isset($res[2])) $icon['size'] = $res[2] ;
//            if (isset($res[3])) $icon['color'] = $res[3] ;
//        }
//
//        return array_merge(array('type'=>'md','size'=>'md','color'=>''),$defaults,$icon);
//
//    }


//    /**
//     * @param $icon array|string
//     */
//    protected function ouat_icon_md($icon) {
//
//        $ics = array('material-icons');
//
//        $sizes = array(
//            'xs' => array('fs'=>'12px','ic'=>'md-18'),
//            'sm' => array('fs'=>'18px','ic'=>'md-18'),
//            'md' => array('fs'=>'24px','ic'=>'md-24'),
//            'lg' => array('fs'=>'36px','ic'=>'md-36'),
//            'xlg' => array('fs'=>'48px','ic'=>'md-48'),
//        );
//
//        $colors = array(
//            'ok' => array('fg'=>'green'),
//            'warning' => array('fg'=>'orange'),
//            'error' => array('fg'=>'red'),
//        );
//
//        $fs = '' ;
//        $fs = isset($sizes[$icon['size']]) ? $sizes[$icon['size']]['fs'] : $icon['size'] ;
//        if (isset($sizes[$icon['size']]['ic'])) {
//            $ics[] = $sizes[$icon['size']]['ic'] ;
//        }
//
//
//        $fg = isset($colors[$icon['color']]) ? $colors[$icon['color']]['fg'] : $icon['color'];
//
//        $html = "" ;
//
////        $icon['stacked'] = 'circle' ;
//        $ic = '' ;
//
//        if (isset($icon['stacked']) && $icon['stacked']) {
//            $html.='<span class="fa-stack fa-lg">
//                <i class="fa fa-'.$icon['stacked'].' fa-stack-2x" style="color:'.$fg.'"></i>' ;
//            $fg = 'white' ;
//            $ic = ' fa-inverse' ;
//            $ics[] = 'fa-stack-1x fa-inverse' ;
//        }
//
//        $html.='<i class="'.implode(' ',$ics).'" style="vertical-align:middle;font-size:'.$fs.';color:'.$fg.'">'.$icon['name'].'</i>' ;
//
//        if (isset($icon['stacked'])) {
//            $html.="</span>" ;
//        }
//
//        return $html ;
//    }
//
//
//    protected function ouat_icon_fa($icon) {
//
//        $sizes = array(
//            'xs' => array('fs'=>'8pt'),
//            'sm' => array('fs'=>'10pt'),
//            'md' => array('fs'=>'12pt'),
//            'lg' => array('fs'=>'14pt'),
//            'xlg' => array('fs'=>'16pt'),
//        );
//
//        $colors = array(
//            'ok' => array('fg'=>'green'),
//            'warning' => array('fg'=>'orange'),
//            'error' => array('fg'=>'red'),
//        );
//
//        $fs = isset($sizes[$icon['size']]) ? $sizes[$icon['size']]['fs'] : $icon['size'] ;
//        $fg = isset($colors[$icon['color']]) ? $colors[$icon['color']]['fg'] : $icon['color'];
//
//        $html = "" ;
//
//        $icon['stacked'] = 'circle' ;
//        $ic = '' ;
//
//        if (isset($icon['stacked'])) {
//            $html.='<span class="fa-stack fa-lg">
//                <i class="fa fa-'.$icon['stacked'].' fa-stack-2x" style="color:'.$fg.'"></i>' ;
//            $fg = 'white' ;
//            $ic = 'fa-stack-1x fa-inverse' ;
//
//        }
//
//        $html.='<i class="fa fa-fw fa-'.$icon['name'].' '.$ic.'" style="font-size:'.$fs.';color:'.$fg.'"></i>' ;
//
//        if (isset($icon['stacked']) && $icon['stacked']) {
//            $html.="</span>" ;
//        }
//
//        return $html ;
//    }

    /**
     * @param $icon array|string
     */
    public function ouat_icon($icon,$defaults = array()) {
        return OX::ouat_icon($icon,$defaults);
//        $icon = $this->formatIcon($icon,$defaults);
//
//        switch($icon['type']) {
//            case 'fa' : return $this->ouat_icon_fa($icon);
//            case 'md' : return $this->ouat_icon_md($icon);
//        }
    }


    /**
     * <a
    href="{{ path('fbt_app_admin_user_index',{ user: item.id }) }}"
    ox="modal"
    {#ox-route = "fbt_pit_planpit_index"#}
    oxp-pit="{{ item.id }}"
    oxo-modal-size="1000x500"
    oxo-close_success="refresh"
    class="btn-floating waves-effect waves-light blue btn-extra-small"><i class="material-icons">edit</i> </a>
     */

    public function enumerable($data) {
        return json_decode(json_encode($data),true); ;
    }

    public function ox_generate_attributes($args)
    {
        return OX::ox_generate_attributes($args);
    }

    public function  ox_link_modal($link_option,$href,$modal_options,$other_options = array()) {
        return OX::ox_link_modal($link_option,$href,$modal_options,$other_options );
    }

    public function createTabsHelper() {
        return new TabsHelper();
    }

    /**
     * @param $object
     * @return \Ouat\CoreBundle\Action\ActionsList
     */
    public function get_document_actions($object) {
        $actions_list = $this->container->get('app.actions_manager')->getActionsFor($object);
        
        return $actions_list ;
    }

    public function render_document_actions($object) {
        return $this->container->get('templating')->render('@OuatUI/material/modal_document_actions.html.twig',array('document'=>$object));
    }

    public function ox_open_document($document,$args = array()) {
        $args['ox'] = 'open_document' ;
        
        $dmg = $this->container->get('app.documents_manager');
        $mg = $dmg->getDocumentManagerFor($document);

        $args['href'] = $mg->getOpenDialogUrl($document);
        
//        foreach($mg->getOpenDialogRouteParameters($document) as $k => $v) {
//            $args['oxp-'.$k] = $v ;
//        }

        return $args ;
    }

    public function render_attributes($attributes) {
        $res = "" ;

        foreach($attributes as $ka => $va)
            if ($va!==null) {
                $res.= $ka.'="'.$va.'" ' ;
            }

        return $res ;
    }

    /**
     * @param \Twig_Template $template
     * @param $context Twig
     * @return ScreenConfig
     */
    public function getScreenConfig($context) {
        return isset($context['_screen']) ? $context['_screen'] : new ScreenConfig();

    }


    public function dureeFilter($minutes) {
        if ($minutes < 1) {
            return "";
        }
        $hours = floor($minutes / 60);
        $minutes = ($minutes % 60);
        return sprintf("%02dH%02d", $hours, $minutes);
    }




}