<?php

/*
 * This file is part of the MopaBootstrapBundle.
 *
 * (c) Philipp A. Mohrenweiser <phiamo@googlemail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ouat\UIBundle\Menu\Factory;

use Knp\Menu\ItemInterface;

/**
 * Decorator for integrating Bootstrap Menus into KnpMenu.
 * (from MopaBoostrapBundle)
 */
class MenuDecorator
{
    /**
     * Builds a menu item based.
     *
     * @param ItemInterface $item
     * @param array         $options
     */
    public function buildItem(ItemInterface $item, array $options)
    {

        if ($options['description']) {
            $item->setLinkAttribute('title',$options['description']);
            $item->setLinkAttribute('data-toggle',"tooltip");
            $item->setLinkAttribute('data-placement',"bottom");


        }

        if ($options['in']) {
            $item->setExtra('in',$options['in']);
            $item->setLinkAttribute('in',$options['in']);
        }

        if ($options['refresh']) {
            $item->setLinkAttribute('on-back','refresh');
        }

        if ($options['module']) {
            $item->setExtra('module',$options['module']);
            $item->setLinkAttribute('module',$options['module']);
        }

        if ($options['as']) {
            $item->setExtra('as',$options['as']);
            $item->setLinkAttribute('as',$options['as']);
        }


        if ($options['navbar']) {
            $item->setChildrenAttribute('class', 'nav navbar-nav');
//            $item->setChildrenAttribute('class', 'nav-icons text-left');
        }

        if ($options['pills']) {
            $item->setChildrenAttribute('class', 'nav nav-pills');
        }

        if ($options['stacked']) {
            $class = $item->getChildrenAttribute('class');
            $item->setChildrenAttribute('class', $class.' nav-stacked');
        }

        if ($options['dropdown-header']) {
            $item
            ->setAttribute('role', 'presentation')
            ->setAttribute('class', 'dropdown-header')
            ->setUri(null);
        }

        if ($options['list-group']) {
            $item->setChildrenAttribute('class', 'list-group');
            $item->setAttribute('class', 'list-group-item');
        }

        if ($options['list-group-item']) {
            $item->setAttribute('class', 'list-group-item');
        }

        if ($options['dropdown']) {
            $item
                ->setUri('#')
                ->setAttribute('class', trim('dropdown '.$item->getAttribute('class')))
                ->setLinkAttribute('class', 'dropdown-toggle')
                ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');

            foreach($item->getChildren() as $child)
                $child->setLinkAttribute('class','dropdown-item' );
//                ->setChildrenAttribute('class', 'dropdown-menu');
//                ->setChildrenAttribute('role', 'menu');

            if ($options['caret']) {
                $item->setExtra('caret', 'true');
            }
        }

        if ($options['divider']) {
            $item
                ->setLabel('')
                ->setUri(null)
                ->setAttribute('role', 'presentation')
                ->setAttribute('class', 'divider');
        }

        if ($options['pull-right']) {
            $className = $options['navbar'] ? 'navbar-right' : 'pull-right';
            $class = $item->getChildrenAttribute('class', '');
            $item->setChildrenAttribute('class', $class.' '.$className);
        }

        if ($options['icon']) {
            $item->setExtra('icon', $options['icon']);
        }



//        if (!$item->hasChildren()) {
            $class = $item->getAttribute('class', '');
            $item->setAttribute('class', $class.' nav-item');
            $class = $item->getLinkAttribute('class');
            $item->setLinkAttribute('class', $class.' nav-link');
//        }
    }

    /**
     * Builds the options for extension.
     *
     * @param array $options
     *
     * @return array $options
     */
    public function buildOptions(array $options)
    {
        return array_merge(array(
            'navbar' => false,
            'pills' => false,
            'stacked' => false,
            'dropdown-header' => false,
            'dropdown' => false,
            'list-group' => false,
            'list-group-item' => false,
            'caret' => false,
            'pull-right' => false,
            'icon' => false,
            'divider' => false,
            'in' => null,
            'module' => null,
            'description' => null,
            'as' => null,
            'refresh' => true
        ), $options);
    }
}
