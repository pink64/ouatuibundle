<?php

/*
 * This file is part of the MopaBootstrapBundle.
 *
 * (c) Philipp A. Mohrenweiser <phiamo@googlemail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ouat\UIBundle\Menu\Factory;

use Knp\Menu\Factory\ExtensionInterface;

/**
 * Extension for integrating Bootstrap Menus into KnpMenu.
 * (from MopaBoostrapBundle)
 */
class MenuExtension extends MenuDecorator implements ExtensionInterface
{
}
