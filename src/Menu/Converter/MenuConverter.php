<?php

/*
 * This file is part of the MopaBootstrapBundle.
 *
 * (c) Philipp A. Mohrenweiser <phiamo@googlemail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Ouat\UIBundle\Menu\Converter;

use Knp\Menu\ItemInterface;
use Ouat\UIBundle\Menu\Factory\MenuDecorator;


/**
 * Converts a Menu to fit CSS classes for the Navbar to be displayed nicely.
 *
 * Currently the menu is not changed, displaying a multi
 * level menu with e.g. list group might lead to unexpected results.
 *
 * Either we implement a flattening option or warn,
 * or ignore this as its done now.
 *
 * @author phiamo <phiamo@googlemail.com>
 */
class MenuConverter extends \Mopa\Bundle\BootstrapBundle\Menu\Converter\MenuConverter
{

    /**
     * Constructor.
     */
    public function __construct()
    {
        
        $this->decorator = new MenuDecorator();
    }


}
