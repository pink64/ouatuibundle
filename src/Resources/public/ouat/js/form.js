$(function(){
    $(document).on('click','div.radio > label, label.radio-inline',function(event){
        var label = $(event.target).closest('label');
        label.prev('input[type=radio]').click();
    });

    $(document).on('click','div.checkbox > label, label.checkbox-inline',function(event){
        var label = $(event.target).closest('label');
        console.log("click on checkbox");
        label.prev('input[type=checkbox]').click();
    });
});