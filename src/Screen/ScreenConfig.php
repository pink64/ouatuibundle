<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 03/06/2016
 * Time: 11:31
 */

namespace Ouat\UIBundle\Screen;

use Ouat\UIBundle\Helper\TableHelper;

class ScreenConfig {

    
    protected $screenColor;

    /**
     * @var boolean|integer
     */
    protected $screenPanel = false ;

    /**
     * @var boolean|integer
     */
    protected $screenPanelWidth = 3;

    /**
     * @var string
     */
    protected $screenIcon = null;

    /**
     * @var string
     */
    protected $screenTitle = null;

    /**
     * @var string
     */
    protected $screenTitle2 = null;

    /**
     * @var string
     */
    protected $pageTitle = null;



    /**
     * @return mixed
     */
    public function getScreenColor()
    {
        return $this->screenColor;
    }

    /**
     * @param mixed $screenColor
     */
    public function setScreenColor($screenColor)
    {
        $this->screenColor = $screenColor;
    }

    /**
     * @return bool|int
     */
    public function getScreenPanel()
    {
        return $this->screenPanel;
    }

    /**
     * @param bool|int $screenPanel
     */
    public function setScreenPanel($screenPanel)
    {
        $this->screenPanel = $screenPanel;
    }

    /**
     * @return bool|int
     */
    public function getScreenPanelWidth()
    {
        return $this->screenPanelWidth;
    }

    /**
     * @param bool|int $screenPanelWidth
     */
    public function setScreenPanelWidth($screenPanelWidth)
    {
        $this->screenPanelWidth = $screenPanelWidth;
    }

    /**
     * @return string
     */
    public function getScreenIcon()
    {
        return $this->screenIcon;
    }

    /**
     * @param string $screenIcon
     */
    public function setScreenIcon($screenIcon)
    {
        $this->screenIcon = $screenIcon;
    }

    /**
     * @return string
     */
    public function getScreenTitle2()
    {
        return $this->screenTitle2;
    }

    /**
     * @param string $screenTitle2
     */
    public function setScreenTitle2($screenTitle2)
    {
        $this->screenTitle2 = $screenTitle2;
    }

    /**
     * @return string
     */
    public function getScreenTitle()
    {
        return $this->screenTitle;
    }

    /**
     * @param string $screenTitle
     */
    public function setScreenTitle($screenTitle)
    {
        $this->screenTitle = $screenTitle;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }

    /**
     * @var TableHelper
     */
    protected $table ;

    /**
     * @return TableHelper
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param TableHelper $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    public function hasTable() {
        return $this->table != NULL ;
    }

    
    protected $page_menu ;
    protected $page_menu_options ;

    /**
     * @return mixed
     */
    public function getPageMenu()
    {
        return $this->page_menu;
    }

    /**
     * @param mixed $page_menu
     */
    public function setPageMenu($page_menu , $options = array())
    {
        $this->page_menu = $page_menu;
        $this->page_menu_options = $options ;
    }

    /**
     * @return mixed
     */
    public function getPageMenuOptions()
    {
        return $this->page_menu_options;
    }

    public function hasPageMenu() {
        return $this->page_menu != NULL ;
    }





}