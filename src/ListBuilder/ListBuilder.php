<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 29/06/2016
 * Time: 10:59
 */


namespace Ouat\UIBundle\ListBuilder;


use Ouat\UIBundle\Helper\TableHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ListBuilder {

    protected $format = array();

    /**
     * @var ContainerInterface
     */
    protected $container ;

    /**
     * @var TableHelper
     */
    protected $tableHelper ;

    protected $data ;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container ;

        $this->configure();
    }

    /**
     * @return array
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    


    protected function addFieldFormat($name,$type,$format = array()) {
        $this->format['fields'][$name] = ['type'=>$type,'options'=>$format] ;

        return $this ;
    }


    public function start() {
        $this->tableHelper = new TableHelper();
        $this->tableHelper->setContainer($this->container);

        foreach($this->format['fields'] as $kf => $vf) {
            switch($vf['type']) {
                case 'embed' :
                    $builder_class = $vf['options']['class'] ;
                    $z = new $builder_class($this->container);

                    foreach($z->getFormat()['fields'] as $kfs => $vfs)
                        $this->addFieldFormat($kf.'.'.$kfs,$vfs['type'],$vfs['options']);

                    unset($this->format['fields'][$kf]);

                    break ;
            }
        }

        return $this ;

    }



    public function add($fieldName,$options = array()) {
        if (isset($this->format['fields'][$fieldName]['options'])) {
            $options = array_merge($this->format['fields'][$fieldName]['options'],$options);
        }

        $type = 'field' ;

        if (isset($this->format['fields'][$fieldName]['type'])) {
            $type = $this->format['fields'][$fieldName]['type'] ;
        }

        $this->tableHelper->{ 'add'.ucfirst($type) }($fieldName,$options);

        return $this ;
    }



    /**
     * @return TableHelper
     */
    public function getTable() {
        return $this->tableHelper;
    }
}