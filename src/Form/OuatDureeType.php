<?php
/**
 * Created by PhpStorm.
 * User: scoton
 * Date: 13/02/2016
 * Time: 13:56
 */

namespace Ouat\UIBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OuatDureeType extends ChoiceType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'choices'   => array(

                "1 H" => 60,
                "2 H" => 120,
                "3 H" => 3*60,
                "4 H" => 4*60,
                "La journée" => 7*60
            )
        ));

//        $resolver->setNormalizer('entry_options', $entryOptionsNormalizer);
    }

//    public function getParent()
//    {
//        return ChoiceType::class;
//    }



}