<?php
/**
 * Created by PhpStorm.
 * User: scoton
 * Date: 13/02/2016
 * Time: 13:56
 */

namespace Ouat\UIBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OuatColorPickerType extends AbstractType
{
    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'ouat_color_picker' ;
    }


//    public function getName()
//    {
//        return 'ouat_date_picker';
//    }


}