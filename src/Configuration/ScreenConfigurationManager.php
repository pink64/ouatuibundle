<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 04/07/2016
 * Time: 16:38
 */

namespace Ouat\UIBundle\Configuration;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Dumper\YamlDumper;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Yaml\Yaml;

class ScreenConfigurationManager {
    /**
     * @var ContainerInterface
     */
    protected $container ;

    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    protected function getScreenConfigurationFile($bundle_name,$screen_name) {
        $base_configuration_dir = $this->container->get('kernel')->getRootDir().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'configuration'.DIRECTORY_SEPARATOR ;
        $config_dir = $base_configuration_dir.$bundle_name.DIRECTORY_SEPARATOR.'screens'.DIRECTORY_SEPARATOR.$screen_name.DIRECTORY_SEPARATOR ;

        $config_file = $config_dir.'screen.yml' ;

        return $config_file ;
    }

    public function createScreenConfiguration($bundle_name,$screen_name) {
        $config_file = $this->getScreenConfigurationFile($bundle_name,$screen_name);
        $config_dir = basename($config_file);

        if (!file_exists($config_dir)) {
            mkdir($config_dir,0755,true);
        }

        if (!file_exists($config_file)) {
            $config = new ScreenConfiguration();

            $data = $this->container->get('jms_serializer')->serialize($config,'array');
            $output = Yaml::dump($data,10);
            file_put_contents($config_file,$output);
        }

        return $config_file ;
    }

    /**
     * @param $bundle_name
     * @param $screen_name
     * @return array|\JMS\Serializer\scalar|mixed|ScreenConfiguration
     */
    public function getScreenConfiguration($bundle_name,$screen_name) {
        $config_file = $this->getScreenConfigurationFile($bundle_name,$screen_name);

        if (!file_exists($config_file)) {
            $config_file = $this->createScreenConfiguration($bundle_name,$screen_name);
        }
        $data = file_get_contents($config_file);
        $data = Yaml::parse($data);
        $config = $this->container->get('jms_serializer')->deserialize($data,ScreenConfiguration::class,'array');
        $config->setConfigFileName($config_file);

        return $config ;
    }

    public function updateScreenConfiguration(ScreenConfiguration $config,$data) {
        $config_file = $config->getConfigFileName();
        $data = $this->container->get('jms_serializer')->serialize($data,'array');
        $output = Yaml::dump($data,10);
        file_put_contents($config_file,$output);
    }

    public function createScreenConfigurationForm($screen) {
        $data = $this->container->get('jms_serializer')->serialize($screen,'array');

        $fb = $this->container->get('form.factory')->createBuilder('Symfony\Component\Form\Extension\Core\Type\FormType',$data)
                ;

        $screen->buildForm($fb);

        return $fb;
    }


}