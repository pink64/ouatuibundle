<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 04/07/2016
 * Time: 16:33
 */

namespace Ouat\UIBundle\Configuration;

use JMS\Serializer\Annotation\Type;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ScreenConfiguration extends BaseConfiguration {

    /**
     * @Type("string")
     */
    protected $theme = 'unique-color white-text';

    /**
     * @Type("boolean")
     */
    protected $withPanel = true;

    /**
     * @Type("boolean")
     */
    protected $panelWidth = 3;

    /**
     * @Type("string")
     */
    protected $title = 'TITRE ECRAN';

    /**
     * @Type("string")
     */
    protected $title2 = 'SOUS-TITRE ECRAN';

    /**
     * @Type("string")
     */
    protected $entity_var = null ;


    /**
     * @Type("array<string,Ouat\UIBundle\Configuration\PageConfiguration>")
     */
    protected $pages = array();

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return mixed
     */
    public function getWithPanel()
    {
        return $this->withPanel;
    }

    /**
     * @param mixed $withPanel
     */
    public function setWithPanel($withPanel)
    {
        $this->withPanel = $withPanel;
    }

    /**
     * @return mixed
     */
    public function getPanelWidth()
    {
        return $this->panelWidth;
    }

    /**
     * @param mixed $panelWidth
     */
    public function setPanelWidth($panelWidth)
    {
        $this->panelWidth = $panelWidth;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle2()
    {
        return $this->title2;
    }

    /**
     * @param mixed $title2
     */
    public function setTitle2($title2)
    {
        $this->title2 = $title2;
    }

    /**
     * @return mixed
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param mixed $pages
     */
    public function setPages($pages)
    {
        $this->pages = $pages;
    }

    public function buildForm(FormBuilderInterface $builder) {
        $builder->add('title',TextType::class,['label'=>"Titre principal"]);
        $builder->add('title2',TextType::class,['label'=>"Sous titre"]);
        $builder->add('theme',TextType::class,['label'=>"Thème"]);
    }

}