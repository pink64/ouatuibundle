<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 04/07/2016
 * Time: 16:34
 */

namespace Ouat\UIBundle\Configuration;

use JMS\Serializer\Annotation as Serializer;

class BaseConfiguration {

    /**
     * @Serializer\Exclude()
     */
    protected $config_file_name ;

    /**
     * @return mixed
     */
    public function getConfigFileName()
    {
        return $this->config_file_name;
    }

    /**
     * @param mixed $config_file_name
     */
    public function setConfigFileName($config_file_name)
    {
        $this->config_file_name = $config_file_name;
    }


}